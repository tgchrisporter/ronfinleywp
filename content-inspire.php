<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage thefinleytheme
 * @since thefinleytheme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php $featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

  <div class="content-block hero" style="background-image: url('<?php echo $featuredImage; ?>')">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <header class="hero--header">
            <h1>
              <span class="text-small sup-header">RonFinley.com</span>
              <span class="large"><?php the_title(); ?></span>
            </h1>
          </header>
        </div>
      </div>
    </div>
  </div>

  <div class="content-block copy">
    <div class="container-fluid">

      <div class="row content-piece-container">
        <div class="col-xs-12">
          <div class="content-piece">

            
          </div>
        </div>
      </div>

    </div>
  </div>

  <?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->
