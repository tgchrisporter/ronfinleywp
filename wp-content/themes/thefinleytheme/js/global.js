var rfjs = rfjs || {},
  body = $('body'),
  menuTrigger = $('.menu-trigger-bars'),
  menuClose = $('.navmenu-close'),
  navMenu = $('.navmenu'),
  clientId = '170233da4cd642e8aa9e487519e99eb9',
  videoSrc = $(".hero-popover-video iframe").attr('src'),
  inspireTrig = $('.cta'),
  inspireBG = $('.inspire-bg'),
  inspireHeight = $('.inspire-bg-container').height(),
  screenHeight = $(window).height();

function createPhotoElement(photo) {
  var innerHtml = $('<img>')
    .addClass('instagram-image')
    .attr('src', photo.images.standard_resolution.url);

  innerHtml = $('<a>')
    .attr('target', '_blank')
    .attr('href', photo.link)
    .append(innerHtml);

  return $('<div>')
    .addClass('item')
    .attr('id', photo.id)
    .append(innerHtml);
}

function didLoadInstagram(event, response) {
  var that = this;

  $.each(response.data, function (i, photo) {
    $(that).append(createPhotoElement(photo));
  });
}

rfjs.main = {
  init: function () {
    // this.instagramJS();
    this.menuTrigger();
    this.videoTrigger();
    this.linksExternal();
    this.footerWaypoint();
    this.mediaFilter();
  },
  mediaFilter: function () {
    var tab = $('.media-type a');
    tab.on("click", function(e){
      var mt = $(this).attr('href');
      e.preventDefault();
      $('.press-list-item').removeClass('active');
      tab.removeClass('active');
      $(this).addClass('active');
      $('.' +mt).addClass('active');
    });
  },
  instagramJS: function () {
    $('.instagram').on('willLoadInstagram', function (event, options) {
      console.log(options);
    });
    $('.instagram').on('didLoadInstagram', function (event, response) {
      console.log(response);
    });
    $('.instagram').on('didLoadInstagram', didLoadInstagram);
    $('.instagram').instagram({
      userId: 518461922,
      accessToken: 'YOUR-ACCESS-TOKEN-HERE',
      count: 6,
      clientId: clientId
    });
  },
  menuTrigger: function () {
    menuTrigger.on('click', function () {
      body.addClass('menu-active');
    });
    menuClose.on('click', function () {
      body.removeClass('menu-active');
    });
  },
  videoTrigger: function () {
    $("a[href$='#playvideo']").click(function () {
      $("html, body").animate({ scrollTop: 0 }, "fast");
      setTimeout(function () {
        $('body').addClass('hero-video-on');
        $('.hero-popover-video-container').html('<iframe src="https://player.vimeo.com/video/129924807?title=1&amp;byline=1&amp;portrait=1&amp;autoplay=true" width="100%" height="700" frameborder="0"></iframe>');
      }, 200);
      return false;
    });

    $(".hero-popover-video-close").click(function () {
      $('body').removeClass('hero-video-on');
      $('.hero-popover-video-container').empty();
    });
  },
  linksExternal: function () {
    $('a').each(function() {
       var a = new RegExp('/' + window.location.host + '/');
       if(!a.test(this.href)) {
           $(this).click(function(event) {
               event.preventDefault();
               event.stopPropagation();
               window.open(this.href, '_blank');
           });
       }
    });
  },
  footerWaypoint: function () {
    inspireBG.css('height', inspireHeight);
    inspireTrig.waypoint(function () {
      inspireBG.toggleClass('footer-in-view');
    }, {
      offset: '100%',
    });
  },
};

$(document).ready(function () {
  rfjs.main.init();
});