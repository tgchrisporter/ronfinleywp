<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	
	<div class="content-block hero" style="background-image: url('<?php bloginfo('template_url'); ?>/img/rf-404-ladybug.jpg')">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <header class="hero--header">
          	<h1>
          		<span class="text-small sup-header">A gangsta can get lost too...</span>
          	  <span class="large"><?php _e( 'This page cannot be found', 'ronfinleywp' ); ?></span>
              <a href="/" class="btn btn-green">Get me back home</a>
          	</h1>
          </header>
        </div>
      </div>
    </div>
  </div>

<?php get_footer(); ?>
