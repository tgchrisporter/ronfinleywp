<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage thefinleytheme
 * @since thefinleytheme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

	<div class="content-block hero" style="background-image: url('<?php echo $featuredImage; ?>')">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <header class="hero--header">
          	<h1>
          		<span class="text-small sup-header"><?php the_title(); ?></span>
          	  <span class="large"><?php the_field('headline'); ?></span>
              <a href="http://ronfinley.us7.list-manage.com/subscribe/post?u=8cec7cbc56eb53ac0f04728a5&id=6a1cbfeecf" target="_blank" class="btn btn-green btn-contact">Volunteer Now</a>
          	</h1>
          </header>
        </div>
      </div>
    </div>
  </div>

	<div class="content-block copy">
    <div class="container-fluid">

      <?php 
        $last = count(get_field('content_block'));
        $x=1;

        if( have_rows('content_block') ): 
      ?>

        <?php while( have_rows('content_block') ): the_row(); 

            // vars
            $rows = get_field('content_block' ); // get all the rows
            $header = get_sub_field('content_block_header');
            $intro = get_sub_field('content_block_intro_copy');
            $content = get_sub_field('content_block_copy');
            $image = get_sub_field('content_block_image');
            ?>

          <?php if($x!=$last): ?>

            <div class="row content-piece-container">
              <div class="col-md-6">
                <div class="content-piece">
                  <header class="content-piece-header">
                    <h2><?php echo $header; ?></h2>
                  </header>

                  <?php if( $intro ): ?>
                    <div class="content-piece--copy large intro intro-md-left"><?php echo $intro; ?></div>
                  <?php endif; ?>

                  <?php if( $content ): ?>
                    <div class="content-piece--copy"><?php echo $content; ?></div>
                  <?php endif; ?>
                  
                </div>
              </div>

              <div class="col-md-6">
                <aside class="content-image remove-sides" style=
                "background-image: url('<?php echo $image; ?>')">
                </aside>
              </div>
            </div>

          <?php else: ?>

            <div class="row content-piece-container content-piece-wide" style="background-image: url('<?php echo $image; ?>')">
              <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="content-piece">
                  <header class="content-piece-header">
                    <h2><?php echo $header; ?></h2>
                  </header>

                  <?php if( $intro ): ?>
                    <div class="content-piece--copy large intro intro-md-left"><?php echo $intro; ?></div>
                  <?php endif; ?>

                  <?php if( $content ): ?>
                    <div class="content-piece--copy"><?php echo $content; ?></div>
                  <?php endif; ?>

                </div>
              </div>
            </div>

          <?php endif; ?>

        <?php $x++; endwhile; ?>

      <?php endif; ?>

    </div>
  </div>

	<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->
