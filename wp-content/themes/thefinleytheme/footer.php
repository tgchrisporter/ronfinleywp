<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage thefinleytheme
 * @since thefinleytheme 1.0
 */
?>

	</section>

  <div class="content-block cta">
    <?php include (TEMPLATEPATH . '/calltoaction.php'); ?>
  </div>

  <section class="footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-6">
          <div class="footer-block footer-nav">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <?php wp_nav_menu( array( 'theme_location' => 'navigation' ) ); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12 col-lg-6">
          <div class="footer-block footer-email remove-sides">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="footer-email-form">
                    <header class="footer-header">
                      <h2>Sign up for our newsletter</h2>
                    </header>

                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                    <form action="//ronfinley.us7.list-manage.com/subscribe/post?u=8cec7cbc56eb53ac0f04728a5&amp;id=6a1cbfeecf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" class="required email footer-email--input footer-email--text" id="mce-EMAIL" placeholder="My Email Address">
                          </div>
                          <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                          </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                          <div style="position: absolute; left: -5000px;"><input type="text" name="b_8cec7cbc56eb53ac0f04728a5_6a1cbfeecf" tabindex="-1" value=""></div>
                        </div>
                        <div class="col-sm-4">
                          <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-orange footer-email--input footer-email--submit"></div>
                          </div>
                        </div>
                      </div>
                    </form>
                    </div>

                    <!--End mc_embed_signup-->
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/output.min.js"></script>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-55511025-1', 'auto');
    ga('send', 'pageview');
  </script>
  <?php wp_footer(); ?>

</body>
</html>
