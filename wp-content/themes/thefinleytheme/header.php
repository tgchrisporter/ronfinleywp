<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage thefinleytheme
 * @since thefinleytheme 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

  <title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"> 
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

  <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php bloginfo('template_directory'); ?>/style.min.css" rel="stylesheet" type="text/css" />
  
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
  <section class="masthead">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-6">
          <a class="masthead-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">Ron Finley</a>
        </div>

        <div class="col-xs-6">
          <div class="masthead-menu-trigger">
            <div class="menu-trigger-bars">
              <div class="menu-trigger-bars-ele"></div>

              <div class="menu-trigger-bars-ele"></div>

              <div class="menu-trigger-bars-ele"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="navmenu skew">
    <ol class="navmenu-options">
      <li>
        <div class="navmenu-close">
          &times;
        </div>
      </li>

      <li>
        <a class="navmenu-social fa fa-twitter" href="https://twitter.com/ronfinleyhq" style=
        "font-style: italic"></a>
      </li>

      <li>
        <a class="navmenu-social fa fa-facebook" href="https://www.facebook.com/TheRonFinleyProject" style=
        "font-style: italic"></a>
      </li>

      <li>
        <a class="navmenu-social fa fa-instagram" href="https://instagram.com/ronfinleyhq" style=
        "font-style: italic"></a>
      </li>
    </ol>

    <div class="navmenu-list">
      <?php wp_nav_menu( array( 'theme_location' => 'navigation' ) ); ?>
    </div>
  </section>

  <section class="content">