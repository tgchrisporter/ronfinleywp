<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage thefinleytheme
 * @since thefinleytheme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php $featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

  <div class="content-block hero" style="background-image: url('<?php echo $featuredImage; ?>')">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <header class="hero--header">
            <h1>
              <span class="text-small sup-header">RonFinley.com</span>
              <span class="large"><?php the_title(); ?></span>
            </h1>
          </header>
        </div>
      </div>
    </div>
  </div>

  <div class="content-block copy">
    <div class="container-fluid">

      <div class="row content-piece-container">
        <div class="col-xs-12">
          <div class="content-piece">

            <ul class="media-type">
              <li><a class="active" href="all">All</a></li>
              <li><a href="talk">Talks</a></li>
              <li><a href="interview">Interviews</a></li>
              <li><a href="article">Articles</a></li>
              <li><a href="video">Videos</a></li>
            </ul>

            <?php if( have_rows('press_item') ): ?>
            <ul class="row press-list">
              <?php while( have_rows('press_item') ): the_row(); ?>
              <li class="col-sm-6 col-md-3 press-list-item all active <?php the_sub_field('media_type'); ?>">
                <div class="press-list-item-inner">
                  <a class="press-link-image" href="<?php the_sub_field('press_link'); ?>">
                    <span class="press-image" style="background-image: url('<?php the_sub_field('press_image'); ?>')"></span>
                  </a>
                  <a class="press-link-text" href="<?php the_sub_field('press_link'); ?>">
                    <span class="press-site"><?php the_sub_field('press_name'); ?></span>
                  </a>
                  <span class="press-date"><?php the_sub_field('press_date'); ?></span>
                  <span class="press-desc"><?php the_sub_field('press_description'); ?></span>
                </div>
              </li>
              <?php endwhile; ?>
            </ul>
            <?php endif; ?>
          </div>
        </div>
      </div>

    </div>
  </div>

  <?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->
