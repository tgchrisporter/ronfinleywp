<section class="cta">
    <div class="cta-inner">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-6 col-md-offset-1">
            <div class="cta-copy">
              <header class="cta-header-small">
                <h2>Flash Sale: Plant some shit t-shirts 30% off, valid from 10/9 - 10/11 only</h2>
              </header>
            </div>
          </div>

          <div class="col-xs-12 col-md-4">
            <a href="http://rfp.bigcartel.com/?utm_source=rf&utm_medium=site&utm_campaign=oct15flashsale" class="btn btn-cta" target="_blank">Shop</a>
          </div>
        </div>
      </div>
    </div>
  </section>