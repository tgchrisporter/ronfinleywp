<?php

  // ----------------------------------------------------------------------------
  // ---------- Translations can be filed in the /languages/ directory ----------
  // ----------------------------------------------------------------------------
  load_theme_textdomain( 'html5reset', TEMPLATEPATH . '/languages' );

  $locale = get_locale();
  $locale_file = TEMPLATEPATH . "/languages/$locale.php";
  if ( is_readable($locale_file) )
      require_once($locale_file);

  // ----------------------------------------------------------
  // ---------- Redirect all RSS feeds to FeedBurner ----------
  // ----------------------------------------------------------
  function custom_feed_link($output, $feed) {
      $feed_url = 'http://feeds.feedburner.com/statesupply';
      $feed_array = array(
          'rss'           => $feed_url,
          'rss2'          => $feed_url,
          'atom'          => $feed_url,
          'rdf'           => $feed_url,
          'comments_rss2' => ''
      );
      $feed_array[$feed] = $feed_url;
      $output = $feed_array[$feed];
      return $output;
  }
  add_filter('feed_link','custom_feed_link', 1, 2);
  
  function other_feed_links($link) {
      $link = 'http://feeds.feedburner.com/statesupply';
      return $link;
  }
  add_filter('category_feed_link', 'other_feed_links');
  add_filter('author_feed_link', 'other_feed_links');

  // ---------------------------------------------------------------
  // ---------- 15-minute publishing buffer for RSS feeds ----------
  // ---------------------------------------------------------------
  function publish_later_on_feed($where) {
      global $wpdb;
      if (is_feed()) {
          // timestamp in WP-format
          $now = gmdate('Y-m-d H:i:s');
          // value for wait; + device
          $wait = '15'; // integer
          $device = 'MINUTE'; // MINUTE, HOUR, DAY, WEEK, MONTH, YEAR
          $where .= " AND TIMESTAMPDIFF($device, $wpdb->posts.post_date_gmt, '$now') > $wait ";
      }
      return $where;
  }
  add_filter('posts_where', 'publish_later_on_feed');
  
  // -----------------------------------------------------
  // ---------- Add RSS links to <head> section ----------
  // -----------------------------------------------------
  automatic_feed_links();

  // --------------------------------------
  // ---------- Remove admin bar ----------
  // --------------------------------------
  show_admin_bar(false);

  // ------------------------------------------------
  // ---------- Add post thumbnail support ----------
  // ------------------------------------------------
  add_theme_support('post-thumbnails');
  add_image_size('large-thumbnail', 246, 140);
  
  // ----------------------------------------------------------------------------
  // ---------- Remove width and height attributes from post thumbnail ----------
  // ----------------------------------------------------------------------------
  function clean_img_width_height($string){
      return preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>',$string);
  }

  // ----------------------------------------------
  // ---------- Add page excerpt support ----------
  // ----------------------------------------------
  add_post_type_support('page', 'excerpt');

  // -----------------------------------------
  // ---------- Clean up the <head> ----------
  // -----------------------------------------
  function removeHeadLinks() {
      remove_action('wp_head', 'rsd_link');
      remove_action('wp_head', 'wlwmanifest_link');
  }
  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');
  
  // --------------------------------------------
  // ---------- Enable sidebar widgets ----------
  // --------------------------------------------
  if (function_exists('register_sidebar')) {
      register_sidebar(array(
          'name' => 'Post Sidebar',
          'description' => 'Place widgets here that you would like to display on the sidebar of the single post page.',
          'before_widget' => '<div class="widget">',
          'after_widget' => '</div>',
          'before_title' => '<h2>',
          'after_title' => '</h2>',
      ));
  }
  
  // ----------------------------------------------------
  // ---------- Enable custom navigation menus ----------
  // ----------------------------------------------------
  if (function_exists('register_nav_menus')) {
      register_nav_menus(array(
          'main_nav' => 'Main Navigation Menu'
      )); 
  }
  
  // ----------------------------------------------------------------------------------
  // ---------- Create the home (homepage) option in custom navigation menus ----------
  // ----------------------------------------------------------------------------------
  function home_page_menu_item($args) {
      $args['show_home'] = true;
      return $args;
  }
  add_filter('wp_page_menu_args', 'home_page_menu_item');
  
  // ------------------------------------------------
  // ---------- Enable Post Format support ----------
  // ------------------------------------------------
  add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

  // ------------------------------------------------
  // ---------- Add class to post excerpts ----------
  // ------------------------------------------------
  function add_excerpt_class( $excerpt ) {
      $excerpt = str_replace( "<p", "<p class=\"excerpt\"", $excerpt );
      return $excerpt;
  }
  add_filter( "the_excerpt", "add_excerpt_class" );

  // ------------------------------------------------------------
  // ---------- Wrap all post images in figure element ----------
  // ------------------------------------------------------------
  function wrap_my_div($html, $id, $caption, $title, $align, $url, $size, $alt){
      return '<figure class="figure">'.$html.'</figure>';
  }
  add_filter('image_send_to_editor', 'wrap_my_div', 10, 8);
           
	// Register Main Menu
	function register_my_menu() {
	  register_nav_menu('navigation',__( 'Navigation' ));
	}
	add_action( 'init', 'register_my_menu' );

	// Register Social Menu
	function register_social_menu() {
	  register_nav_menu('socialnav',__( 'Social Nav' ));
	}
	add_action( 'init', 'register_social_menu' );