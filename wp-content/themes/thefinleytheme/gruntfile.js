module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    copy: {
      dev: {
        src: 'readme.txt',
        dest: 'README.md'
      }
    },

    uglify: {
      dist: {
        files: {
          'js/output.min.js': ['js/jquery-1.11.3.min.js', 'js/jquery.waypoints.min.js', 'js/instagram.min.js', 'js/global.js']
        }
      }
    },

    sass: {
      dev: {
        options: {
          style: 'expanded',
        },
        files: {
          'style.css': 'scss/style.scss',
        },
      },
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'style.min.css': 'scss/style.scss',
        }
      }
    },

    postcss: {
      options: {
        map: true, // inline sourcemaps

        processors: [
          require('pixrem')(),
          require('autoprefixer-core')({browsers: 'last 2 versions'}), // add vendor prefixes
        ]
      },
      dist: {
        src: '**/*.scss'
      }
    },

    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass' ],
        options: {
          livereload: true,
        },
      },
      scripts: {
        files: 'js/*.js',
        tasks: ['uglify'],
        options: {
          debounceDelay: 250,
          livereload: true,
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['watch', 'postcss', 'uglify', 'sass', 'copy']);
};