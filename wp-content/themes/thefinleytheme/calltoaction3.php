<section class="cta">
    <div class="cta-inner">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-6 col-md-offset-1">
            <div class="cta-copy">
              <header class="cta-header-small">
                <h2>You're invited to the Garden Soiree: the private screening of 'Can You Dig This', presented by the Ron Finley Project</h2>
              </header>
            </div>
          </div>

          <div class="col-xs-12 col-md-4">
            <a href="https://www.eventbrite.com/e/ron-finley-projects-garden-soiree-the-private-screening-of-can-you-dig-this-tickets-18834290871" class="btn btn-cta btn-cta-3" target="_blank">RSVP Today</a>
          </div>
        </div>
      </div>
    </div>
  </section>