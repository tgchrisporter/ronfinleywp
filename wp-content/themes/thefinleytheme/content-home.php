<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage thefinleytheme
 * @since thefinleytheme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php $featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

  <div class="content-block hero" style="background-image: url('<?php echo $featuredImage; ?>')">
    <div class="hero-content-container">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="hero--content">
              <header class="hero--header">
                <h1>
                  <span class="text-small sup-header"><?php the_field('hero_sup_headline'); ?></span>
                  <span class="large"><?php the_field('hero_headline'); ?></span>
                </h1>
              </header>
              <a class="btn btn-play" href="<?php the_field('call_to_action_location'); ?>" onclick="ga('send', 'event', { eventCategory: 'TED Talk', eventAction: 'View Video', eventLabel: 'Clicked'});"><i class="fa fa-play"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="hero-html5video">
      <video class="hero-html5video-control" autoplay="true" loop="true">
        <!-- MP4 must be first for iPad! -->
        <source src="<?php bloginfo('template_url'); ?>/video/canyoudigthis-video-1.mp4" type="video/mp4" /><!-- Safari / iOS video    -->
        <source src="<?php bloginfo('template_url'); ?>/video/canyoudigthis-video-1.ogv" type="video/ogg" /><!-- Firefox / Opera / Chrome10 -->
        <!-- fallback to Flash: -->
      </video>
    </div>

    <div class="hero-popover-video">
      <div class="hero-popover-video-close"><i class="fa fa-times"></i></div>
      <div class="hero-popover-video-container"></div>
    </div>
  </div>
  <div class="content-block cta">
    <?php include (TEMPLATEPATH . '/calltoaction.php'); ?>
  </div>
  <div class="content-block feed">
    <div class="container-fluid">
      <div class="row content-piece-container">
        <div class="col-sm-8">

          <div class="row home-featured-videos">
            <div class="col-sm-12">
              <header>
                <h3>Featured Videos</h3>
              </header>
            </div>
            <div class="col-sm-6 home-featured-videos-ele">
              <iframe width="100%" height="330" src="https://www.youtube.com/embed/Y7hnhC54WqA" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-sm-6 home-featured-videos-ele">
              <iframe width="100%" height="330" src="https://www.youtube.com/embed/owDCnGmhdAs" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-sm-12">
              <footer class="content-footer">
                <a class="sidebar-item-link content-piece--cta" href="press-media">View more Videos</a>
              </footer>
            </div>
          </div>

          <div class="row ig-feed">
            <header>
              <h3><a href="https://instagram.com/explore/tags/plantsomeshit/">@RonFinleyHQ</a> on Instagram</h3> <span>Inspiration Is Everywhere.</span>
            </header>
            <div class="col-sm-12">
              <div class="instagram" id="ronfinleyhq"></div>
            </div>
            <div class="col-sm-12">
              <footer class="content-footer">
                <a class="sidebar-item-link content-piece--cta" href="https://instagram.com/explore/tags/plantsomeshit/">View more IG Photos</a>
              </footer>
            </div>
          </div>
        </div>

        <div class="col-sm-4 home-sidebar">
          <aside class="sidebar">
            <div class="sidebar-item">
              <h3 class="sidebar-item-header">Who's Ron Finley?</h3><a class=
              "sidebar-item-link content-piece--cta" href="meet-ron-finley">Read More</a>
            </div>
            <div class="sidebar-item">
              <h3 class="sidebar-item-header">Learn about the Ron Finley Project</h3><a class=
              "sidebar-item-link content-piece--cta" href="the-ron-finley-project">Read More</a>
            </div>
            <div class="sidebar-item">
              <h3 class="sidebar-item-header">Get dirty in our gear</h3><a class=
              "sidebar-item-link content-piece--cta" href="http://rfp.bigcartel.com/">Shop Now</a>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </div>

  <?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->
