<section class="cta">
    <div class="cta-inner">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-6 col-md-offset-1">
            <div class="cta-copy">
              <header class="cta-header">
                <h2>Help us grow in more communities</h2>
              </header>
            </div>
          </div>

          <div class="col-xs-12 col-md-4">
            <div class="donate-button">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="QD2DYB4PP4UVL">
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>